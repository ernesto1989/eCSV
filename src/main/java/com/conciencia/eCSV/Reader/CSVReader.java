package com.conciencia.eCSV.Reader;

import com.conciencia.eCSV.Reader.Exception.UnscapedSeparator;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * CSVReader : Main Class of the eCSV - Reading section.
 * This class provides the main configuration for the user to read
 * CSV files.
 * 
 ****************************************************************
 *
 * Must be instanciated providing:
 *
 * 1.- csvFile: the file path. this is an obligatory property
 * 2.- csvSeparator: file separator.
 * 3.- ignoreHeaders: flag that indicates that the headers line (the first line)
 *     must be ignored.
 *
 * Defined Constructors.
 *
 * public CSVReader(String csvFile);
 * public CSVReader(String csvFile,String csvSeparator)
 * public CSVReader(String csvFile, Boolean ignoreHeaders)
 * public SimpleCSVReader(String csvFile,String csvSeparator, Boolean ignoreHeaders)
 *
 *
 * How to:
 *
 * 1.- Implement a class that extends CSVReader, providing a class
 *     que represents the file lines.
 * 2.- Override the private method map(String[] readedLine) which, given an array
 *     of Strings (String[]) that holds a line of the file, must return a list of 
 *     objects of the class that represents the file lines. 
 * 3.- Create an instance of the class mentioned at 1.
 * 4.- Execute readFile(), to read the file
 * 5.- Execute getListOfObjects(),  which will return the list of objects
 *
 *
 * @author Ernesto Cantu Valle 
 * ernesto.cantu1989@live.com
 * Conciencia
 * 09/02/2017
 * 
 * ------------------------------------------------------------------------
 *
 * Mainteinance
 * 
 * Observations:
 * Developer:
 * Date: 
 */
public abstract class CSVReader<T> {
    
    /* File path */
    private final String csvFile; 

    /* Separator */
    private final String csvSeparator; 

    /* List of readed lines */
    protected List<String[]> readedLines;

    /* Ignore headers line */
    private final Boolean ignoreHeaders;

    /* Scape char to ignore separator inside field */
    private final Character quote = '"';
    
    private static final Logger LOG = Logger.getLogger(CSVReader.class.getName());
    
    

    /* CONSTRUCTORS */
    
    /**
     * Receives the file path.
     * Considerates "," as separator and does not ignores
     * the first line.
     * @param csvFile path
     */
    public CSVReader(String csvFile){
        LOG.setLevel(Level.INFO);
        this.csvSeparator = ",";
        this.readedLines = new ArrayList<>(); 
        this.csvFile = csvFile;
        this.ignoreHeaders = false;
    }

    /**
     * Receives the file path and the csv separator char.
     * Does not ignores the first line
     * @param csvFile file path 
     * @param csvSeparator file separator
     */
    public CSVReader(String csvFile, String csvSeparator){
        LOG.setLevel(Level.INFO);
        this.csvSeparator = csvSeparator;
        this.readedLines = new ArrayList<>(); //Lista de arrays de campos (Representa a cada uno de los registros leidos ya separados)
        this.csvFile = csvFile;   
        this.ignoreHeaders = false;
    }

    /**
     * Constructor that receives the file path and the ignoreHeaders flag.
     * Uses the comma as separator
     * 
     * @param csvFile file path
     * @param ignoreHeaders ignore headers flag
     */
    public CSVReader(String csvFile, Boolean ignoreHeaders){
        LOG.setLevel(Level.INFO);
        this.csvSeparator = ",";
        this.readedLines = new ArrayList<>(); //Lista de arrays de campos (Representa a cada uno de los registros leidos ya separados)
        this.csvFile = csvFile;   
        this.ignoreHeaders = ignoreHeaders;
    }

    /**
     * Constructor that receives the hole config of the class.
     * 
     * @param csvFile file path
     * @param csvSeparator file separator
     * @param ignoreHeaders ignore headers flag
     */
    public CSVReader(String csvFile, String csvSeparator, Boolean ignoreHeaders){
        LOG.setLevel(Level.INFO);
        this.csvSeparator = csvSeparator;
        this.readedLines = new ArrayList<>(); //Lista de arrays de campos (Representa a cada uno de los registros leidos ya separados)
        this.csvFile = csvFile;   
        this.ignoreHeaders = ignoreHeaders;
    }

    /******************************************************************************************************************************/

    /**
     * Reads the file with the given configuration.
     * 
     * Pseudocode:
     *
     * 1.- Creates a BufferedReader to read the file
     * 2.- Ejecutes iterateOverFileLines providing the buffered reader.
     * 3.- Closes the BR
     * 4.- Ends the file reading
     *
     * @return true or false, depending on the file reading status
     *
     * @author Ernesto Cantu Valle 
     * ernesto.cantu1989@live.com
     * Conciencia
     * 09/02/2017
     */
    public boolean readFile() {
        LOG.warning("WARNING: Remember to use \" quotes to scape the separator in the fields.\n");
        LOG.info("Reading the given file.\n");

        BufferedReader fileReader = null;
        try {
            fileReader = new BufferedReader(new FileReader(csvFile));
            iterateOverFileLines(fileReader);
        } catch (FileNotFoundException e) {
            LOG.severe("File Not Found!\n");
            return false;
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "{0}\n", e.getMessage());
            return false;
        } finally {
            if (fileReader != null) {
                try {
                    LOG.info("Closing fileReader\n");
                    fileReader.close();
                } catch (IOException e) {
                    LOG.log(Level.SEVERE, "{0}\n", e.getMessage());
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Receives a buffered reader and iterates over the file lines.
     * 
     * For each line, executes splitLine  to convert the array to an
     * array of strings and stored the String[] on readedLines.
     *
     * Pseudocode:
     *
     * 1.- Creates a boolean to control the ignoreHeaders option.
     * 2.- For each readed line:
     *     2.1.- if the flag given in 1 is true:
     *           2.1.1.- Turn the flag to false
     *           2.1.2.- Continue with the next iteration
     *     2.2.- Split the readed line creating the String[] array and storing
     *           the array on readedLines.
     *
     * @param fileReader the buffered reader that will read the file
     *
     * @author Ernesto Cantu Valle 
     * ernesto.cantu1989@live.com
     * Conciencia
     * 09/02/2017
     */
    private void iterateOverFileLines(BufferedReader fileReader) throws FileNotFoundException,IOException{
        String line;
        Boolean isFirst = this.ignoreHeaders;

        while ((line = fileReader.readLine()) != null) {
            if( isFirst ) {
                LOG.info("Ignoring headers.\n");
                isFirst = false;
                continue;
            }
            this.readedLines.add(splitLine(line));
        }

    }

    /**
     * Method that converts the line readed to an array of strings (String[]) scaping
     * the separator if is correctly quoted.
     * 
     * Pseudocode:
     *
     * 1.- Creates a flag to detect if the quotes are opened or closed.
     * 2.- Converts the line to an array of chars (char[])
     * 3.- For each char in the array:
     *     3.1.- If the char equals to the quote char,I change the state of the flag given in 1
     *     3.2.- If the quotes are closed and the char is equal to the separator,
     *           the field should be stored on an array list
     *     3.3.- If the quotes are opened, then the method keeps appending the char to a String builder.
     * 4.- Finishing the loop defined in 3, the last field readed is stored in the array list
     * 5.- Returns the array list as a String[].
     *
     * 
     * @param line line from the file
     * @return the String[] that represents the line
     *
     *
     * @author Ernesto Cantu Valle 
     * ernesto.cantu1989@live.com
     * Conciencia
     * 09/02/2017
     */
    private String[] splitLine(String line){
        boolean openedQuote = false;
        StringBuilder builder = new StringBuilder();
        ArrayList<String> arrayList = new ArrayList<>();
        char[] lineAsCharArray = line.toCharArray();

        for(int i = 0; i<lineAsCharArray.length;i++){
            if(compareTwoChars(quote,lineAsCharArray[i])){
                openedQuote = !openedQuote; //abro o cierro comillas segun el caso
                continue;
            }
            if( !openedQuote &&  compareTwoChars(csvSeparator.charAt(0),lineAsCharArray[i]) ){
                appendValueToArray(builder, arrayList);
                continue;
            }
            builder.append(lineAsCharArray[i]);
        }
        appendValueToArray(builder, arrayList);
        return convertArrayListToArray(arrayList);
    }

    
    /**
     * Private method that helps comparing 2 chars
     * @param c1 char 1
     * @param c2 char 2 
     * @return son iguales regresa true, no son iguales regresa false
     */
    private boolean compareTwoChars(Character c1,char c2){
        return c1.equals(c2);
    }
    
    /**
     * Private method that takes a String Builder, converts it to a String, and
     * stores the string on the array list
     * @param s the StringBuilder
     * @param a the ArrayList
     */
    private void appendValueToArray(StringBuilder s,ArrayList<String> a){
        a.add(s.toString());
        s.setLength(0);
    }

    /**
     * Private method that converts an ArrayList to an array of strings (String[]).
     * 
     * @param arrayList the array list to convert
     * @return the array of strings[]
     */
    private String[] convertArrayListToArray(ArrayList<String> arrayList){
        String[] array = new String[arrayList.size()];
        array = arrayList.toArray(array);
        return array;
    }


    /*************************************************************************/

    /**
     * Method that returns the list of objects readed from the file.
     * 
     * Pseudocode:
     * 
     * 1.- Creates a list that will contain the readed objects
     * 2.- For each String in the array readedLines
     *     2.1.- Get the array
     *     2.2.- Execute map, which will return the object mapped
     *     2.3.- Store the object on readedObjects.
     * 3.- Returns the list of ReadedObjects.
     *
     * @return the list of readed objects
     * 
     * @author Ernesto Cantu Valle 
     * ernesto.cantu1989@live.com
     * Conciencia
     * 09/02/2017
     */
    public List<T> getListOfObjects(){
        List<T> readedObjects = new ArrayList<>();
        String[] readedLine;

        for(int i = 0; i< this.readedLines.size();i++){
            readedLine = this.readedLines.get(i);
            try{
                readedObjects.add(map(readedLine));
            }catch(UnscapedSeparator e){
                LOG.log(Level.SEVERE,"Unscaped separator on line {0}", new Object[]{this.ignoreHeaders?i+1:i});
            }
        }
        return readedObjects;
    }

    /**
     * Abstract method that, given an objet of type T and an array of Strings,
     * maps an array to an object.
     * 
     * Must be overriten
     *
     * @param readedLine the array of strings
     * @return the object of type T (Should be defined by the user)
     * @throws com.conciencia.eCSV.Reader.Exception.UnscapedSeparator 
     * Error that ocurres when the file is not scaped.
     * 
     * @author Ernesto Cantu Valle 
     * ernesto.cantu1989@live.com
     * Conciencia
     * 09/02/2017
     */
    public abstract T map(String[] readedLine) throws UnscapedSeparator;
}
package com.conciencia.eCSV.Reader.Exception;

/**
 * Excepcion class that can be used by any other class that extends CSVReader
 *
 * @author Ernesto Cantu Valle 
 * ernesto.cantu1989@live.com
 * Conciencia
 * 14/01/2017
 */
public class UnscapedSeparator extends Exception{
	
	public UnscapedSeparator(String message){
		super(message);
	}
}
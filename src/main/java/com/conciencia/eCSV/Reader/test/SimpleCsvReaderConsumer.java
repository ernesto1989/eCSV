package com.conciencia.eCSV.Reader.test;

import com.conciencia.eCSV.Reader.CSVReader;
import java.util.List;

/**
 * This is the client code that determines the usage of eCSV on production.
 * You should create a class that extends CSVReader (overriding the map method)
 * to get the hole power of eCSV. 
 * 
 * To use it on a client code, just create a new reader, read the file and get the
 * list of objects.
 *
 * @author Ernesto Cantu Valle 
 * ernesto.cantu1989@live.com
 * Conciencia
 * 14/01/2017 
 */
public class SimpleCsvReaderConsumer{
	
    public static void main(String[] args){
    	CSVReader reader = new SimpleCSVReaderTest("C:/CSVFile.csv");
    	boolean readed = reader.readFile();

    	if(readed){
            List<CsvFileObject> objects = reader.getListOfObjects();
            for(CsvFileObject o:objects){
                System.out.println(o);
            }	
    	}
    }
}
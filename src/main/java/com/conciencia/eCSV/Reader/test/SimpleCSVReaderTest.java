package com.conciencia.eCSV.Reader.test;


import com.conciencia.eCSV.Reader.CSVReader;
import com.conciencia.eCSV.Reader.Exception.UnscapedSeparator;

/**
 * Example of CSVReader Implementations
 */
public class SimpleCSVReaderTest extends CSVReader<CsvFileObject> {

	public SimpleCSVReaderTest(String csvFile){
		super(csvFile,true);
	}

   /**
    * This method must be overriten by the child code. Defines how to convert
    * the array to an object.
    * 
    * Tipically here you will iterate the array, verify the size of the array
    * use readed fields to set them to the object.
    * 
    * @param readedLine
    * @return
    * @throws UnscapedSeparator 
    */
    @Override
    public CsvFileObject map(String[] readedLine) throws UnscapedSeparator{
        CsvFileObject object = new CsvFileObject();
        int j = 0;
        if(readedLine.length == 2){
            object.setCode(readedLine[j++]);
            object.setCountry(readedLine[j++]);
        }else{
            throw new UnscapedSeparator("");
        }
        return object;
    }
}
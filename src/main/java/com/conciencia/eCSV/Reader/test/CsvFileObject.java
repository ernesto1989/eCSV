package com.conciencia.eCSV.Reader.test;

public class CsvFileObject {

    private String code;
    private String country;

    public CsvFileObject(){}

    public String getCode(){
            return this.code;
    }

    public void setCode(String code){
            this.code = code;
    }

    public String getCountry(){
            return this.country;
    }

    public void setCountry(String country){
            this.country = country;
    }

    @Override
    public String toString(){
            return "CsvFileObject: {'" + this.getCode() + "','" + this.getCountry() + "'}";
    }
}
Pure Java Version of eCSV.

Same concept: 

1.- User extends CsvReader abstract class
2.- Overrides the map method
3.- Calls the readFile method to read the file
4.- Calls the obtainListOfObjects method to get the list of readed objects.
  
To see an example, please refer th CSVReader class Docs, or to the example located on 
package com.conciencia.eCSV.Reader.test;

How to study the test:

CSVFile.csv is the csv file that must be readed.
CSVFileObject is the class that represents the file in java. (A POJO)
SimpleCSVReaderTest is the class that extends CSVReader. Overrides the map method.
  
  author: Ernesto Cantu Valle
  ernesto.cantu1989@live.com
  https://github.com/ernesto1989
  https://github.com/ernesto1989/eCSV
